//
//  FirstViewController.swift
//  clickerTest
//
//  Created by Nicolas Spragg on 2016-05-07.
//  Copyright © 2016 Nicolas Spragg. All rights reserved.
//

import UIKit
var byteCount = 0.0
var storageAmount = 512.0; // Player starts with 512 bytes
class FirstViewController: UIViewController {
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        var timer = NSTimer()
        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(FirstViewController.autoTap), userInfo: nil, repeats: true)
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBOutlet var amountDownloadedLabel: UILabel!

    @IBOutlet var storageAmountLabel: UILabel!
    @IBOutlet var costAmountLabel: UILabel!
    @IBAction func downloadButton(sender: AnyObject) {
        
        if byteCount < storageAmount {
            byteCount += tapUpgradeCombo
            amountDownloadedLabel.text = String(format: "%0.1f", byteCount)
        
        }
        
        

    }
    
    func autoTap () {
        if byteCount < storageAmount {
        byteCount += tapsPerSecond
        amountDownloadedLabel.text = String(format: "%0.1f", byteCount)
        }
    }
    
    


    @IBOutlet var unitLabel: UILabel!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

