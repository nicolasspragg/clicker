//
//  SecondViewController.swift
//  clickerTest
//
//  Created by Nicolas Spragg on 2016-05-07.
//  Copyright © 2016 Nicolas Spragg. All rights reserved.
//

import UIKit
var tapUpgradeCombo = 1.0
var tapUpgradeCost = 50.0
var tapsPerSecond = 0.0
var tapsPerSecondCost = 50.0
class SecondViewController: UIViewController {
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBOutlet var tapsPerSecondCostLabel: UILabel!
    @IBOutlet var costLabel: UILabel!
    
    @IBAction func tapUpgradePressed(sender: AnyObject) {
        if byteCount >= tapUpgradeCost {
            byteCount -= tapUpgradeCost
            tapUpgradeCombo += 1
            
            tapUpgradeCost *= 3
            
            costLabel.text = String(format: "%0.3f",tapUpgradeCost)
            
        }
    }
    
    @IBAction func tapsPerSecondIncrease(sender: AnyObject) {
        if byteCount >= tapsPerSecondCost {
            byteCount -= tapsPerSecondCost
            tapsPerSecond += 1
            
            tapsPerSecondCost *= 3
            
            tapsPerSecondCostLabel.text = String(format:"%0.3f",tapsPerSecondCost)
        
    }
    }
  

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

